chrome.runtime.onMessage.addListener(function(request, sender) {
  if (request.action == "getSource") {
    message.innerText = request.source;
  }
});

function onWindowLoad() {

  var message = document.querySelector('#message');

  chrome.tabs.executeScript(null, {
    file: "payload.js"
  }, function() {
    // If you try and inject into an extensions page or the webstore/NTP you'll get an error
    if (chrome.runtime.lastError) {
      message.innerText = 'There was an error injecting script : \n' + chrome.runtime.lastError.message;
    }
  });


  
    
    var button = document.getElementById("mybutton");
    if(button){
      button.addEventListener("click", function($scope) {
        	var result = message.innerText;
            //var result = JSON.stringify(message.innerText);
             
            var filename = "download.txt";
            // var filename1 = "download.csv";
            download(filename, result);
            // download(filename1, result);
          }, false);
    }



}

function download(filename, text) {
  
    var element = document.createElement('a');
 	// var headers = ["A"].join(",");
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

window.onload = onWindowLoad;

