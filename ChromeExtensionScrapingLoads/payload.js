// @author Rob W <http://stackoverflow.com/users/938089/rob-w>
// Demo: var serialized_html = DOMtoString(document);

function DOMtoString(document_root) {
    var html = '',
        node = document_root.firstChild;
    
    while (node) {
        switch (node.nodeType) {
        case Node.ELEMENT_NODE:
            html += node.outerHTML;
            break;
        case Node.TEXT_NODE:
            html += node.nodeValue;
            break;
        case Node.CDATA_SECTION_NODE:
            html += '<![CDATA[' + node.nodeValue + ']]>';
            break;
        case Node.COMMENT_NODE:
            html += '<!--' + node.nodeValue + '-->';
            break;
        case Node.DOCUMENT_TYPE_NODE:
            // (X)HTML documents are identified by public identifiers
            html += "<!DOCTYPE " + node.name + (node.publicId ? ' PUBLIC "' + node.publicId + '"' : '') + (!node.publicId && node.systemId ? ' SYSTEM' : '') + (node.systemId ? ' "' + node.systemId + '"' : '') + '>\n';
            break;
        }
        node = node.nextSibling;
    }
   	
   	
    var elements = document.getElementsByClassName('addressLabel GCR0MSHCLDB addressFrom label');
    var randAd = [];
    var i;
 	for (i = 0; i < elements.length; ++i) {
        var arr = [];

		var addressFrom = document.getElementsByClassName('addressLabel GCR0MSHCLDB addressFrom label');
		var addressTo = document.getElementsByClassName('addressLabel GCR0MSHCLDB addressTo label');
		var addressVia = document.getElementsByClassName('addressLabel GCR0MSHCLDB addressVia label');
		var pickupTime = document.getElementsByClassName('pickupTime');
		var deliverTime = document.getElementsByClassName('deliverTime');
		var viaTime = document.getElementsByClassName('viaTime');
		var distance = document.getElementsByClassName('gwt-Label GCR0MSHCOL distance');
		var stackable = document.getElementsByClassName('gwt-Label GCR0MSHCLDB stackable');
		var weight = document.getElementsByClassName('gwt-Label GCR0MSHCLDB weight');
		var packaging = document.getElementsByClassName('gwt-Label GCR0MSHCLDB packaging');
		var dimensions = document.getElementsByClassName('dimensions');
		var statusBlockHeaderLabel = document.getElementsByClassName('gwt-Label GCR0MSHCLWB GCR0MSHCPSC statusBlockHeaderLabel');
		var loadViewStatus = document.getElementsByClassName('gwt-Label GCR0MSHCEHB GCR0MSHCKP loadViewStatus GCR0MSHCLGB');
		var postedAt = document.getElementsByClassName('GCR0MSHCK2 GCR0MSHCKP GCR0MSHCKPB postedAt');
		var reference = document.getElementsByClassName('gwt-InlineLabel reference');
		var vehicleIcon = document.getElementsByClassName('gwt-Image GCR0MSHCLDB label vehicleIcon');
		var carDescr = document.getElementsByClassName('gwt-Label GCR0MSHCLDB carDescr');
		var paymentTerms = document.getElementsByClassName('gwt-Label GCR0MSHCLDB paymentTerms sss');
		var notes = document.getElementsByClassName('gwt-HTML GCR0MSHCEUB GCR0MSHCLDB GCR0MSHCIBB notes');

		var elements2 = document.getElementsByClassName('GCR0MSHCON centerSection');
      		
  		arr[0] = 'FROM|'+addressFrom.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[1] = 'To|'+addressTo.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[2] = 'Via|'+addressVia.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[3] = 'Pickup|'+pickupTime.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[4] = 'Deliver|'+deliverTime.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[5] = 'Via Time|'+viaTime.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[6] = 'Dist|'+distance.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[7] = 'Stackable|'+stackable.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[8] = 'Weight|'+weight.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[9] = 'Packaging|'+packaging.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[10] = 'Dims|'+dimensions.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[11] = 'Status|'+statusBlockHeaderLabel.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[12] = 'Posted by|'+loadViewStatus.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[13] = 'Posted At|'+postedAt.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[14] = 'Load ID|'+reference.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[16] = 'Requested|'+carDescr.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[17] = 'Payment Terms|'+paymentTerms.item(i).innerText.replace(/\s+/g,' ').trim()+'||';
  		arr[18] = 'Load Notes|'+notes.item(i).innerText.replace(/\s+/g,' ').trim()+'\n';
    	
        /*var array3 = elements1.item(i).innerText;
     	array31 = array3.replace(/\s+/g,' ').trim();
     	array32 = array31.replace(/\:\s+/g,'|').trim();
     	arr[0] = array32.replace(/\,\s+/g,'||').trim();
       
        var array5 = elements2.item(i).innerText;
     	array51 = array5.replace(/\s+/g,' ').trim();
     	array52 = array51.replace(/\:\s+/g,'|').trim();
     	arr[2] = array52.replace(/\,\s+/g,'||').trim();*/
              
        var obj = arr.join('');
        randAd.push(obj);
    
    }
    
    console.log(randAd);
  	return randAd;
   /* var myJSON = JSON.stringify(randAd);
	return myJSON;*/
    /*var elem = elements.item(0).innerHTML;
    var array = elem.split(":");
    var myJSON = JSON.stringify(array);
    var my_json = myJSON.replace( /[\r\n]+/gm, "" ); 
    console.log(elements);*/
    //console.log(elements.item(0).innerText);

    /*var data_e = elements.item(0).innerText+elements.item(1).innerText*/
}

chrome.runtime.sendMessage({
    action: "getSource",
    source: DOMtoString(document)
});